package com.example.axon.query;

import com.example.axon.coreapi.event.CreateUserEvent;
import com.example.axon.coreapi.event.DeleteUserEvent;
import com.example.axon.coreapi.event.UpdateUserEvent;
import com.example.axon.coreapi.query.FindAllUserQuery;
import com.example.axon.coreapi.query.FindUserByIdQuery;
import com.example.axon.gui.dto.UserDto;
import com.example.axon.gui.mapper.UserMapper;
import com.example.axon.coreapi.persistent.entity.User;
import com.example.axon.coreapi.persistent.repository.UserRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;
    private final UserMapper mapper;

    @EventHandler
    public void on(CreateUserEvent event) {
        repository.save(new User(event.getId(), event.getName(), event.getStatus()));
    }

    @EventHandler
    public void on(UpdateUserEvent event) {
        Optional<User> optionalUser = repository.findById(event.getId());
        if (optionalUser.isPresent()) {
            var user = optionalUser.get();
            user.setName(event.getName());
            user.setStatus(event.getStatus());
            repository.save(user);
        }
    }

    @EventHandler
    public void on(DeleteUserEvent event) {
        Optional<User> optionalUser = repository.findById(event.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setStatus(event.getStatus());
            repository.save(user);
        }
    }

    @QueryHandler
    public List<UserDto> findAll(FindAllUserQuery query) {
        return mapper.asUserDtos(repository.findAll());
    }

    @QueryHandler
    public UserDto findById(FindUserByIdQuery query) {
        return mapper.asUserDto(repository.findById(query.getId()).orElse(null));
    }

}
