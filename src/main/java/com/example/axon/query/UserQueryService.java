package com.example.axon.query;

import com.example.axon.coreapi.query.FindAllUserQuery;
import com.example.axon.coreapi.query.FindUserByIdQuery;
import com.example.axon.gui.dto.UserDto;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserQueryService {

    private final QueryGateway gateway;

    public CompletableFuture<List<UserDto>> findAll() {
        return gateway.query(new FindAllUserQuery(),
                ResponseTypes.multipleInstancesOf(UserDto.class));
    }

    public CompletableFuture<UserDto> findById(UUID id) {
        return gateway.query(new FindUserByIdQuery(id), ResponseTypes.instanceOf(UserDto.class));
    }
}
