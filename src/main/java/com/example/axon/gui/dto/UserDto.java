package com.example.axon.gui.dto;

import com.example.axon.command.Status;
import lombok.Data;

@Data
public class UserDto {
    private String name;
    private Status status;
}
