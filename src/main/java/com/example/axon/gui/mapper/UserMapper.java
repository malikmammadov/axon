package com.example.axon.gui.mapper;

import com.example.axon.gui.dto.UserDto;
import com.example.axon.coreapi.persistent.entity.User;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    List<UserDto> asUserDtos(List<User> users);

    UserDto asUserDto(User user);
}
