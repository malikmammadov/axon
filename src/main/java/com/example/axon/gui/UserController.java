package com.example.axon.gui;

import com.example.axon.gui.dto.UserDto;
import com.example.axon.query.UserQueryService;
import com.example.axon.command.UserCommandService;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserCommandService commandService;
    private final UserQueryService queryService;

    @PostMapping
    public UUID create(@RequestBody UserDto user) {
        return commandService.create(user);
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") UUID id,
                       @RequestBody UserDto user) {
        commandService.update(id, user);
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable("id") UUID id) {
        commandService.delete(id);
    }

    @GetMapping
    public CompletableFuture<List<UserDto>> findAll() {
        return queryService.findAll();
    }

    @GetMapping("{id}")
    CompletableFuture<UserDto> findById(@PathVariable("id") UUID id) {
        return queryService.findById(id);
    }

}
