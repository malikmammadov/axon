package com.example.axon.coreapi.persistent.entity;

import com.example.axon.command.Status;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@Data
@Document(collection = "users")
public class User {

    @Id
    private UUID id;
    private String name;
    private Status status;
}
