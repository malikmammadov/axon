package com.example.axon.coreapi.persistent.repository;

import com.example.axon.coreapi.persistent.entity.User;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, UUID> {
}
