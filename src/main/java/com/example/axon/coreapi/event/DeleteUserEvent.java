package com.example.axon.coreapi.event;

import com.example.axon.command.Status;
import java.util.UUID;
import lombok.Getter;

@Getter
public class DeleteUserEvent extends BaseEvent<UUID> {

    public DeleteUserEvent(UUID id, Status status) {
        super(id, status);
    }
}
