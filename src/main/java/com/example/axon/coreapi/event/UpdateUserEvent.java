package com.example.axon.coreapi.event;

import com.example.axon.command.Status;
import java.util.UUID;
import lombok.Getter;

@Getter
public class UpdateUserEvent extends BaseEvent<UUID> {

    private final String name;

    public UpdateUserEvent(UUID id, String name, Status status) {
        super(id, status);
        this.name = name;
    }
}
