package com.example.axon.coreapi.event;

import com.example.axon.command.Status;
import lombok.Getter;

@Getter
public class BaseEvent<T> {

    private final T id;
    private final Status status;

    public BaseEvent(T id, Status status) {
        this.id = id;
        this.status = status;
    }
}
