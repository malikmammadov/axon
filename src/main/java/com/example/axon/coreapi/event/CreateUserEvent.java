package com.example.axon.coreapi.event;

import com.example.axon.command.Status;
import java.util.UUID;
import lombok.Getter;

@Getter
public class CreateUserEvent extends BaseEvent<UUID> {

    private final String name;

    public CreateUserEvent(UUID id, String name, Status status) {
        super(id, status);
        this.name = name;
    }
}
