package com.example.axon.coreapi.command;

import java.util.UUID;
import lombok.Getter;

@Getter
public class DeleteUserCommand extends BaseCommand<UUID> {

    public DeleteUserCommand(UUID id) {
        super(id);
    }
}
