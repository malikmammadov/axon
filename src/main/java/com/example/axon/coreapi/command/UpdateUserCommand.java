package com.example.axon.coreapi.command;

import java.util.UUID;
import lombok.Getter;

@Getter
public class UpdateUserCommand extends BaseCommand<UUID> {

    public final String name;

    public UpdateUserCommand(UUID id, String name) {
        super(id);
        this.name = name;
    }
}
