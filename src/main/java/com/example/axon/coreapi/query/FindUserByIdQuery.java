package com.example.axon.coreapi.query;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class FindUserByIdQuery {

    private UUID id;
}
