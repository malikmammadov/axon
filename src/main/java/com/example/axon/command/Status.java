package com.example.axon.command;

public enum Status {
    CREATED, UPDATED, DELETED
}
