package com.example.axon.command;

import com.example.axon.coreapi.command.CreateUserCommand;
import com.example.axon.coreapi.command.DeleteUserCommand;
import com.example.axon.coreapi.command.UpdateUserCommand;
import com.example.axon.gui.dto.UserDto;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserCommandService {

    private final CommandGateway gateway;

    public UUID create(UserDto user) {
        return gateway.sendAndWait(new CreateUserCommand(UUID.randomUUID(), user.getName()));
    }

    public void update(UUID id, UserDto user) {
        gateway.send(new UpdateUserCommand(id, user.getName()));
    }

    public void delete(UUID id) {
        gateway.send(new DeleteUserCommand(id));
    }
}
