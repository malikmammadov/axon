package com.example.axon.command;

import com.example.axon.coreapi.command.CreateUserCommand;
import com.example.axon.coreapi.command.DeleteUserCommand;
import com.example.axon.coreapi.command.UpdateUserCommand;
import com.example.axon.coreapi.event.CreateUserEvent;
import com.example.axon.coreapi.event.DeleteUserEvent;
import com.example.axon.coreapi.event.UpdateUserEvent;
import java.util.UUID;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@NoArgsConstructor
public class UserAggregate {

    @AggregateIdentifier
    private UUID id;
    private Status status;

    // create
    @CommandHandler
    public UserAggregate(CreateUserCommand command) {
        this.id = command.getId();
        AggregateLifecycle.apply(new CreateUserEvent(id, command.getName(), Status.CREATED));
    }

    @EventSourcingHandler
    protected void on(CreateUserEvent event) {
        this.id = event.getId();
        this.status = event.getStatus();
    }

    // update
    @CommandHandler
    protected void on(UpdateUserCommand command) {
        AggregateLifecycle.apply(new UpdateUserEvent(id, command.getName(), Status.UPDATED));
    }

    @EventSourcingHandler
    protected void on(UpdateUserEvent event) {
        this.id = event.getId();
        this.status = event.getStatus();
    }

    // delete
    @CommandHandler
    protected void on(DeleteUserCommand command) {
        AggregateLifecycle.apply(new DeleteUserEvent(id, Status.DELETED));
    }

    @EventSourcingHandler
    protected void on(DeleteUserEvent event) {
        this.id = event.getId();
        this.status = event.getStatus();
    }
}
